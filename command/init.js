'use strict'
const exec = require('child_process').exec
const co = require('co')
const prompt = require('co-prompt')
const chalk = require('chalk')

const templateUrl = 'git@gitlab.com:zhong_qi/vue-koa-template.git'

module.exports = () => {
    co(function *() {
        // 处理用户输入
        let projectName = yield prompt('Project name: ')
        if (!projectName) {
            console.log(chalk.red('\n × projectName cannot be empty!'))
            process.exit()
        }
        let cmdStr = `git clone ${templateUrl} ${projectName}`
        console.log(chalk.white('\n Start generating...'))
        exec(cmdStr, (error, stdout, stderr) => {
            if (error) {
                console.log(error)
                process.exit()
            }
            console.log(chalk.green('\n √ Generation completed!'))
            console.log(`\n cd ${projectName} && npm install \n`)
            process.exit()
        })
    });
}
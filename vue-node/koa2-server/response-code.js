const responseCode = {
    SUCCESS: 0,
    LOGIN_FAILED: 1
}

module.exports = responseCode;
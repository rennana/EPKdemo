const router = require('koa-router')();
const GoodsModel = require('../models/goods');
const UserModel = require('../models/users');

router.prefix('/goods')

// 查询商品列表数据
router.get('/list', async (ctx, next) => {
    let page = parseInt(ctx.query.page);
    let pageSize = parseInt(ctx.query.pageSize);
    let priceLevel = ctx.query.priceLevel;
    let sort = ctx.query.sort; // sort是排序的意思，取值为1和-1
    let skip = (page - 1) * pageSize;
    let priceGt = '';
    let priceLte = '';
    let params = {};
    if (priceLevel != 'all') {
        switch (priceLevel) {
            case '0': priceGt = 0; priceLte = 100; break;
            case '1': priceGt = 100; priceLte = 500; break;
            case '2': priceGt = 500; priceLte = 1000; break;
            case '3': priceGt = 1000; priceLte = 5000; break;
        }
        params = {
            salePrice: {
                $gt: priceGt,
                $lte: priceLte
            }
        }
    }
    let goods = await GoodsModel.find(params).skip(skip).limit(pageSize).sort({'salePrice': sort});
    ctx.body = {
        status: '0',
        msg: '',
        result: {
            count: goods.length,
            list: goods
        }
    }
});

router.post('/addCart', async (ctx, next) => {
    const userId = ctx.cookies.get('userId');
    const productId = ctx.request.body.productId;
    const user = await UserModel.findOne({ userId, userId });
    let goodsItem = '';
    user.cartList.forEach(function (item) {
        if (item.productId == productId) {
            goodsItem = item;
            item.productNum++;
        }
    });
    if (goodsItem) {
        await user.save();
    } else {
        goodsItem = await GoodsModel.findOne({ productId: productId});
        goodsItem.productNum = 1;
        goodsItem.checked = 1;
        user.cartList.push(goodsItem);
        await user.save();
    }
    ctx.body = {
        status: '0',
        msg: '',
        result: 'suc'
    }
});

module.exports = router;
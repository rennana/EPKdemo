const router = require('koa-router')()
const UserModel = require('../models/users');
require('../util/util');
const CODE = require('../response-code');

router.prefix('/users')

// 登录
router.post('/login', async (ctx, next) => {
  const params = {
    userName: ctx.request.body.userName,
    userPwd: ctx.request.body.userPwd
  }
  const user = await UserModel.findOne(params);
  if (user) {
    ctx.cookies.set('userId', user.userId, {
      path: '/',
      maxAge: 1000 * 60 * 60
    });
    ctx.cookies.set('userName', user.userName, {
      path: '/',
      maxAge: 1000 * 60 * 60
    });
    ctx.body = {
      status: CODE.SUCCESS,
      msg: '',
      result: {
        userName: user.userName
      }
    }
  } else {
    ctx.body = {
      status: CODE.LOGIN_FAILED,
      msg: 'no such user'
    };
  }
})

// 登出
router.post('/logout', (ctx, next) => {
  ctx.cookies.set('userId', '', {
    path: '/',
    maxAge: -1
  });
  ctx.body = {
    status: "0",
    msg: '',
    result: ''
  }
})

// 检查登录状态
router.get('/checkLogin', (ctx, next) => {
  if (ctx.cookies.get('userId')) {
    ctx.body = {
      status: '0',
      msg: '',
      result: ctx.cookies.get('userName') || ''
    }
  } else {
    ctx.body = {
      status: '1',
      msg: '未登录',
      result: ''
    }
  }
});

// 获取购物车中货品总数
router.get('/getCartCount', async (ctx, next) => {
  if (ctx.cookies.get('userId')) {
    const userId = ctx.cookies.get('userId');
    const user = await UserModel.findOne({userId, userId});
    let cartList = user.cartList;
    let cartCount = 0;
    cartList.map(function (item) {
      cartCount += parseFloat(item.productNum);
    });
    ctx.body = {
      status: "0",
      msg: "",
      result: cartCount
    };
  } else {
    ctx.body = {
      status: "0",
      msg: "当前用户不存在"
    }
  }
});

//查询当前用户的购物车数据
router.get('/cartList', async (ctx, next) => {
  if (ctx.cookies.get('userId')) {
    const userId = ctx.cookies.get('userId');
    const user = await UserModel.findOne({ userId, userId });
    ctx.body = {
      status: "0",
      msg: "",
      result: user.cartList
    };
  } else {
    ctx.body = {
      status: "0",
      msg: "当前用户不存在"
    }
  }
});

// 购物车删除
router.post('/cartDel', async (ctx, next) => {
  const userId = ctx.cookies.get('userId');
  const productId = ctx.request.body.productId;
  const result = await UserModel.update(
  {userId: userId},
  {$pull: {'cartList': {'productId': productId}}});
  ctx.body = {
    status: '0',
    msg: '',
    result: 'suc'
  };
});

// 修改商品数量
router.post('/cartEdit', async (ctx, next) => {
  const userId = ctx.cookies.get('userId');
  let body = ctx.request.body;
  const productId = body.productId;
  const productNum = body.productNum;
  const checked = body.checked;
  const result = await UserModel.update(
    { "userId": userId, "cartList.productId": productId }, 
    {
      "cartList.$.productNum": productNum,
      "cartList.$.checked": checked,
    }
  );
  ctx.body = {
    status: '0',
    msg: '',
    result: 'suc'
  }
});

router.post('/editCheckAll', async (ctx, next) => {
  const userId = ctx.cookies.get('userId');
  const checkAll = ctx.request.body.checkAll ? '1' : '0';
  const user = await UserModel.findOne({userId: userId});
  if (user) {
    user.cartList.forEach(item => {
      item.checked = checkAll;
    });
    try {
      await user.save();
      ctx.body = {
        status: '0',
        msg: '',
        result: 'suc'
      }
    } catch (error) {
      ctx.body = {
        status: '1',
        msg: '内部错误',
        result: ''
      }
    }
  } else {
    ctx.body = {
      status: '1',
      msg: '用户不存在',
      result: ''
    }
  }
});

// 查询用户地址接口
router.get('/addressList', async (ctx, next) => {
  const userId = ctx.cookies.get('userId');
  const user = await UserModel.findOne({ userId: userId });
  if (user) {
    ctx.body = {
      status: '0',
      msg: '',
      result: user.addressList
    }
  } else {
    ctx.body = {
      status: '1',
      msg: '用户不存在',
      result: ''
    }
  }
});

//设置默认地址接口
router.post('/setDefault', async (ctx, next) => {
  const userId = ctx.cookies.get('userId');
  const addressId = ctx.request.body.addressId;
  if (!addressId) {
    ctx.body = {
      status: '1003',
      msg: 'addressId is null',
      result: ''
    }
  } else {
    const user = await UserModel.findOne({ userId: userId });
    user.addressList.forEach(item => {
      if (item.addressId == addressId) {
        item.isDefault = true;
      } else {
        item.isDefault = false;
      }
    });
    await user.save();
    ctx.body = {
      status: '0',
      msg: '',
      result: ''
    }
  }
});

// 删除地址接口
router.post('/delAddress', async (ctx, next) => {
  const userId = ctx.cookies.get('userId');
  const addressId = ctx.request.body.addressId;
  try {
    await UserModel.update(
      { userId: userId },
      { $pull: { 'addressList': { 'addressId': addressId } } }
    );
    ctx.body = {
      status: '0',
      msg: '',
      result: ''
    }
  } catch (error) {
    ctx.body = {
      status: '1',
      msg: 'inner error' + error,
      result: ''
    }
  }
});

// payMent
router.post('/payMent', async (ctx, next) => {
  const userId = ctx.cookies.get('userId');
  const addressId = ctx.request.body.addressId;
  const orderTotal = ctx.request.body.orderTotal;
  const user = await UserModel.findOne({userId: userId});
  let address = '';
  let goodsList = [];
  //获取当前用户的地址信息
  user.addressList.forEach(item => {
    if (addressId == item.addressId) {
      address = item;
    }
  });
  //获取用户购物车的购买商品
  user.cartList.filter((item) => {
    if (item.checked == '1') {
      goodsList.push(item);
    }
  });
  const platform = '622';
  const r1 = Math.floor(Math.random() * 10);
  const r2 = Math.floor(Math.random() * 10);
  const sysDate = new Date().Format('yyyyMMddhhmmss');
  const createDate = new Date().Format('yyyy-MM-dd hh:mm:ss');
  const orderId = platform + r1 + sysDate + r2;
  const order = {
    orderId: orderId,
    orderTotal: orderTotal,
    addressInfo: address,
    goodsList: goodsList,
    orderStatus: '1',
    createDate: createDate
  };
  user.orderList.push(order);
  await user.save();
  ctx.body = {
    status: "0",
    msg: '',
    result: {
      orderId: order.orderId,
      orderTotal: order.orderTotal
    }
  }
  // try {
  //   await user.save();
  //   ctx.body = {
  //     status: "0",
  //     msg: '',
  //     result: {
  //       orderId: order.orderId,
  //       orderTotal: order.orderTotal
  //     }
  //   }
  // } catch (error) {
  //   ctx.body = {
  //     status: "1",
  //     msg: error.message,
  //     result: ''
  //   }
  // }
});

//根据订单Id查询订单信息
router.get('/orderDetail', async (ctx, next) => {
  const userId = ctx.cookies.get('userId');
  const orderId = ctx.query.orderId;
  const user = await UserModel.findOne({userId: userId});
  const orderList = user.orderList;
  if (orderList.length > 0) {
    let orderTotal = 0;
    orderList.forEach(item => {
      if (item.orderId == orderId) {
        orderTotal = item.orderTotal;
      }
    });
    if (orderTotal > 0) {
      ctx.body = {
        status: '0',
        msg: '',
        result: {
          orderId: orderId,
          orderTotal: orderTotal
        }
      }
    } else {
      ctx.body = {
        status: '120002',
        msg: '无此订单',
        result: ''
      }
    }
  } else {
    ctx.body = {
      status: '120001',
      msg: '当前用户未创建订单',
      result: ''
    }
  }
});

module.exports = router
